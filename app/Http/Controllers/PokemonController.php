<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Composer;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Log;


class PokemonController extends Controller
{
    protected $base_url = 'https://pokeapi.co/api/v2/';

     public function index(Request $request){

     $client = new Client([

         'base_uri' => $this->base_url,
         'timeout'  => 2.0,
     ]);
    $request->offset? $offset = $request->offset : $offset = '0';
    $request->limit? $limit = $request->limit : $limit = '20';
    $response = $client->request('GET', 'pokemon?offset='.$offset.'&limit='.$limit);
    //Log::info('pokemon?offset='.$offset.'&limit='.$limit);
    $posts = json_decode ($response->getBody()->getContents());
    //dd($posts);

    return view('index',compact('posts'));
    }
    /*****************************************************/
    public function show($id)
    {
        $client = new Client([

            'base_uri' => $this->base_url,
            'timeout'  => 2.0,
        ]);


    $response = $client->request('GET', 'pokemon/'.$id);
    $posts = json_decode ($response->getBody()->getContents());

try {
    $response2 = $client->request('GET', 'characteristic/'.$id);
    $posts2 = json_decode ($response2->getBody()->getContents());
} catch (ClientException $e) {

    if ($e->hasResponse()) {
        // ddd($e);
        $res = $e->getResponse()->getStatusCode();
        if($res=='404'){
            $posts2 = [];
        }
    }
}

       return view('detalles',compact('posts','posts2'));

    }

    public function home(){
        return redirect('/pokemon');
    }


    public function ability($id)
    {
        $client = new Client([

            'base_uri' => $this->base_url,
            'timeout'  => 2.0,
        ]);



        $response = $client->request('GET', 'ability/'.$id);

       $posts = json_decode ($response->getBody()->getContents());
       //dd($posts);
       return view('prueba',compact('posts'));
    }
}
