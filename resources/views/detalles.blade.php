<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="jumbotron">
    <div class="container">
    <h3>Detalle Pokemon{{'-'.$posts->name}}</h3>


    {{-- <h3>Im&aacute;genes</h3> --}}
        <table>
            <tr>
                <td><img src="{{$posts->sprites->back_default}}" alt="pokeimage" class="img-thumbnail"></td>
                <td>
                    @if(isset($posts->sprites->back_female))
                    <img src="{{$posts->sprites->back_female}}" alt="pokeimage" class="img-thumbnail">
                    @endif
                </td>
                <td><img src="{{$posts->sprites->back_shiny}}" alt="pokeimage" class="img-thumbnail"></td>

                <td>@if(isset($posts->sprites->back_shiny_female))
                    <img src="{{$posts->sprites->back_shiny_female}}" alt="pokeimage" class="img-thumbnail">
                    @endif
                </td>
                <td><img src="{{$posts->sprites->front_default}}" alt="pokeimage" class="img-thumbnail"></td>
                <td>
                    @if(isset($posts->sprites->front_female))
                    <img src="{{$posts->sprites->front_female}}" alt="pokeimage" class="img-thumbnail">
                    @endif
                </td>
                <td><img src="{{$posts->sprites->front_shiny}}" alt="pokeimage" class="img-thumbnail"></td>
                <td>
                    @if(isset($posts->sprites->front_shiny_female))
                    <img src="{{$posts->sprites->front_shiny_female}}" alt="pokeimage" class="img-thumbnail">

                    @endif
                </td>
            </tr>
        </table>

        <table class="table table-hover table-bordered">
                <thead class="thead-dark">
                        <tr>
                          <th>Tipo</th>
                        </tr>
                        <tbody>
                            @foreach ($posts->types as $post)
                            <tr>
                                <td>{{$loop->iteration}}-.{{ $post->type->name }}</td>
                            </tr>
                            @endforeach
                        </tbody>
            </table>

<table class="table table-hover table-bordered">
        <thead class="thead-dark">
                <tr>
                  <th>Caracteristica</th>

                </tr>
                <tbody>
                    @if($posts2)
                    @foreach ($posts2->descriptions as $post)
                    <tr>
                        @if($post->language->name=='en')
                        <td>{{$post->description}}</td>
                        @endif
                    </tr>
                    @endforeach
                    @endif
                </tbody>
    </table>

    <table class="table table-hover table-bordered">
        <thead class="thead-dark">
                <tr>
                  <th>Habilidades</th>
                  <th>Detalle Habilidad</th>
                </tr>
                <tbody>
                    @foreach ($posts->abilities as $post)
                    <tr>
                    <td>{{$loop->iteration}}-.{{ $post->ability->name }}</td>
                    <td><a href={{url('/prueba/'.substr(substr($post->ability->url,34),0,-1))}} class="badge badge-success">Habilidad</a></td>
                    </tr>
                    @endforeach
                </tbody>
    </table>

    <table class="table table-hover table-bordered">
            <thead class="thead-dark">
                    <tr>
                      <th>Status</th>
                    </tr>
                    <tbody>
                        @foreach ($posts->stats as $post)
                        <tr>
                            <td>{{$loop->iteration}}-.{{ $post->stat->name }}</td>
                        </tr>
                        @endforeach
                    </tbody>
        </table>




    <table class="table table-hover table-bordered">
            <thead class="thead-dark">
                    <tr>

                      <th>Movimientos</th>
                    </tr>
                    <tbody>
                        @foreach ($posts->moves as $post)
                        <tr>

                            <td>{{$loop->iteration}}-.{{ $post->move->name }}</td>
                        </tr>
                        @endforeach
                    </tbody>
        </table>


    </div>
</div>

</body>
</html>

