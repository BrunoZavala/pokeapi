<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pokeapi</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h3>Pokemones</h3>
    <table class="table table-sm">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Detalles</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($posts->results as $post)
          <tr>
          <td>
              <a href="{{url('/pokemon/'.substr(substr($post->url,34),0,-1))}}">{{$post->name}}</a>
          </td>
          </tr>
        @endforeach
        </tbody>
      </table>

<div class="container">
<a href="{{url('/'.substr($posts->previous,26))}}" class="btn btn-success">Anterior</a>
<a href="{{url('/'.substr($posts->next,26))}}" class="btn btn-success">Siguiente</a>
</div>



</div>
</body>
</html>

