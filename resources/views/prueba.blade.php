<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>

<div class="container">
<table class="table table-sm">
    <thead class="thead-dark">
        <tr>

            <th>Nombre</th>

        </tr>
    </thead>
    <tbody>
        @foreach ($posts->effect_entries as $post)
        <tr>
        <td>{{$post->effect}}</td>
        </tr>
        @endforeach


    </tbody>
</table>
</div>
</body>
</html>
